import * as React from 'react';
import { StyleSheet, Text, View, StatusBar, SafeAreaView, ScrollView, Button, TextInput, TouchableWithoutFeedback, Keyboard, Pressable  } from "react-native";
import Constants from 'expo-constants';
 
export default function App() {
  const [valueGasolina, onChangeGasolina] = React.useState(0);
  const [valueAlcool, onChangeAlcool] = React.useState(0);
  const [instructions, onChangeInstructions] = React.useState('Preencha os campos e clique no botão abaixo para descobrir a melhor opção para o seu carro!');
  const [displayResult, onChangeResult] = React.useState('');
  const [porcentagem, onChangePorcentagem] = React.useState(null);
  var result = 0;
 
  /*
    Basta dividir o preço do litro do etanol pelo da gasolina. Se o resultado for inferior a 0,7, o derivado da cana-de-açúcar é o melhor para abastecer. 
    Se for maior que 0,7, então a gasolina é melhor.
  */
  const calculate = () => {
    Keyboard.dismiss()
 
    if(valueAlcool != 0 && valueGasolina != 0) {
      result = valueAlcool/valueGasolina;
      onChangePorcentagem(result * 100)
      onChangeAlcool(0)
      onChangeGasolina(0)
 
      if(result < 0.7) {
        onChangeResult('Álcool')
      } else {
        onChangeResult('Gasolina')
      }
    }
    else {
      alert('Preencha todos os campos corretamente')
    }
  
  }
 
  return (
   <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    <ScrollView>
      <View style={styles.container}>
        <SafeAreaView>
          <Text style={styles.paragraph}>
            Custo benefício 
          </Text>
          <Text style={styles.title}>
            Gasolina x Álcool
          </Text>
 
          <Text style={styles.instructions}>
            {instructions}
          </Text>
        
          <Text style={styles.label}>
            Preço da gasolina (R$)
          </Text>
          <TextInput
            style={styles.input}
            onChangeText={onChangeGasolina}
            value={valueGasolina}
            placeholder="Digite aqui"
            keyboardType="numeric"
          />
 
          <Text style={styles.label}>
            Preço do Álcool (R$)
          </Text>
          <TextInput
            style={styles.input}
            onChangeText={onChangeAlcool}
            value={valueAlcool}
            placeholder="Digite aqui"
            keyboardType="numeric"
          />
 
 
          <Pressable style={styles.button} onPress={calculate}>
            <Text style={styles.textButton}>Calcular</Text>
          </Pressable>
 
          <View style={styles.resultContainer}> 
            {displayResult != '' &&
            <Text style={styles.resultSucess}>
              O valor do álcool é {porcentagem}% do valor da gasolina. {'\n'} O combustível mais vantajoso é {''}
              <Text style={styles.combustivelText}>
               {displayResult}
              </Text>
            </Text>
            }
          </View>
          
        </SafeAreaView>
      </View>
    </ScrollView>
   </TouchableWithoutFeedback>
    
  );
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
    position: 'relative',
  },
  title: {
    fontSize: 32,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  paragraph: {
    margin: 2,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
    instructions: {
    marginVertical: 30,
    marginHorizontal: 20,
    fontSize: 18,
    color: 'blue',
    textAlign: 'center',
  },
  label: {
    marginLeft: 20,
    fontSize: 16,
    fontWeight: 'bold',
  },
  input: {
    height: 40,
    marginTop: 10,
    marginBottom: 30,
    marginHorizontal: 20,
    borderBottomWidth: 1,
    padding: 10,
    borderRadius: 6
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 14,
    paddingHorizontal: 32,
    marginHorizontal: 20,
    borderRadius: 6,
    elevation: 3,
    backgroundColor: 'black',
  },
  textButton: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.75,
    color: 'white',
  },
  resultContainer: {
    borderWidth: 1,
    borderTopEndRadius: 40,
    borderBottomStartRadius: 40,
    marginHorizontal: 20,
    marginTop: 50,
    padding: 10,
    height: 150,
    justifyContent: 'center'
  },
  resultSucess: {
    fontSize: 18,
    textAlign: 'center',
  },
  combustivelText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'green',
  }
});

